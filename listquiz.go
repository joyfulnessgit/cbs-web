package web

import (
	"bitbucket.org/joyfulness/cbs/service"
	ace "bitbucket.org/plimble/ace"
	"fmt"
	"github.com/zenazn/goji/param"
	"github.com/zenazn/goji/web"
	"net/http"
	"strconv"
)

type ListAnswer struct {
	Formname string            `json:"formname" bson:"formname"`
	Answers  map[string]string `json:"answer" bson:"answer"`
}

func GoWebReport(c web.C, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)
	userid := session.Values["user_id"].(string)
	qr := service.NewReportService()
	qr.CreateUserReport(userid)
	session.Values["curquestion_id"] = "finish"
	session.Save(r, w)
	urlPath := "/" + service.RoundList.Current + "/" + userid + "/report"
	http.Redirect(w, r, urlPath, http.StatusFound)
}

func WebListQuizWithAnswer(c web.C, w http.ResponseWriter, r *http.Request) {

	if c.URLParams["user_id"] == "" {
		saveFlash(`กรุณาลองใหม่อีกครั้ง`, w, r)
		http.Redirect(w, r, `/`, http.StatusFound)
	}

	qs := service.NewQuizService()
	listquestion, listanswer, err := qs.GetUserQuestion(c.URLParams["user_id"])

	if err != nil {
		saveFlash(`ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง`, w, r)
		http.Redirect(w, r, `/`, http.StatusFound)
		return
	}

	qus := service.NewUserService()
	user, er := qus.GetCurrentRoundUser(c.URLParams["user_id"])

	if er != nil {
		saveFlash(`กรุณาลองใหม่อีกครั้ง`, w, r)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if user.IsDone == false {
		saveFlash(`ไม่สามารถดูเฉลยได้เนื่องคุณทำข้อสอบไม่เสร็จสมบูรณ์`, w, r)
		http.Redirect(w, r, `/`, http.StatusFound)
		return
	}

	p := struct {
		QuestionsDept service.QuestionsDept
		UserAnswer    map[string]*service.UserAnswer
		FlashMessage  interface{}
	}{QuestionsDept: *listquestion, UserAnswer: listanswer, FlashMessage: ""}
	ace.NoCache(w)
	ace.ServeTemplate(w, `quizwithanswer`, p)
}

func WebListquiz(c web.C, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)

	if session.IsNew {
		saveFlash(`กรุณาลองใหม่อีกครั้ง`, w, r)
		http.Redirect(w, r, `/`, http.StatusFound)
	}

	qs := service.NewQuizService()
	listquestion, listanswer, err := qs.GetUserQuestion(session.Values["user_id"].(string))
	if err == nil && listquestion == nil {
		GoWebReport(c, w, r)
	} else {
		if listquestion == nil {
			saveFlash("คุณไม่สามารถเข้าทำแบบทดสอบได้ เนื่องจากไม่ได้อยู่ในกลุ่มงานที่ต้องสอบ", w, r)
			http.Redirect(w, r, `/`, http.StatusFound)
			return
		}

		session.Values["totalquestion"] = len(listquestion.Questions)

		qus := service.NewUserService()
		user, er := qus.GetCurrentRoundUser(session.Values["user_id"].(string))
		if er != nil {
			saveFlash(er.Error(), w, r)
			http.Redirect(w, r, "/", http.StatusFound)
		} else if user.IsDone == true {
			GoWebReport(c, w, r)
			return
		}
		// else {
		flashes := session.Flashes()
		session.Save(r, w)
		p := struct {
			QuestionsDept service.QuestionsDept
			UserAnswer    map[string]*service.UserAnswer
			FlashMessage  interface{}
		}{QuestionsDept: *listquestion, UserAnswer: listanswer, FlashMessage: flashes}
		ace.NoCache(w)
		ace.ServeTemplate(w, `quiz`, p)
		// }
	}

}

func continueExam(c web.C, w http.ResponseWriter, r *http.Request) {

	if service.IsRoundExpired(service.GetCurrentRound()) {
		saveFlash("ท่านไม่สามารถทำแบบทดสอบได้เนื่องจากอยู่นอกเหนือเวลาที่กำหนด", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	}

	qus := service.NewUserService()
	user, er := qus.GetCurrentRoundUser(r.FormValue("id"))
	checkround := service.GetCurrentRound()
	if er == nil && user.Round == checkround && user.IsDone == false {
		saveUserSession(user, w, r)
		http.Redirect(w, r, "/quiz", http.StatusFound)
	} else if er == nil && user.Round == checkround && user.IsDone == true {
		session, _ := ace.Session(r)
		flashes := session.Flashes()
		fmt.Println(flashes)
		saveFlash("ผู้ใช้นี้ได้ทำแบบทดสอบเสร็จสิ้นแล้ว", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		saveFlash("ไม่พบข้อมูลผู้ใช้", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

func answerQ(questionnumber string, answernumber string, r *http.Request) error {
	//PREPARE DATA
	session, _ := ace.Session(r)
	qs := service.NewQuizService()
	var useranswer service.AnswerForm

	//SET QUESTION ID
	useranswer.QuestionID = questionnumber

	//SET USER
	useranswer.UserID = session.Values["user_id"].(string)
	//SET ANSWER
	useranswer.UserAnswer, _ = strconv.Atoi(answernumber)
	useranswer.UserAnswer = useranswer.UserAnswer
	//SEND ANSWER
	err2 := qs.Answer(&useranswer)

	return err2
}

func finishExam(c web.C, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)
	r.ParseForm()
	listans := ListAnswer{}
	errx := param.Parse(r.Form, &listans)

	if len(listans.Answers) != session.Values["totalquestion"] || errx != nil {
		saveFlash(errx.Error(), w, r)
		ace.NoCache(w)
		http.Redirect(w, r, "/quiz", http.StatusFound)
		return
	}

	userid := session.Values["user_id"].(string)
	qr := service.NewReportService()
	err := qr.CreateUserReport(userid)

	if err != nil {
		saveFlash(err.Error(), w, r)
		ace.NoCache(w)
		http.Redirect(w, r, "/quiz", http.StatusFound)
		return
	}

	session.Values["curquestion_id"] = "finish"
	session.Save(r, w)
	urlPath := "/" + service.RoundList.Current + "/" + userid + "/report"
	http.Redirect(w, r, urlPath, http.StatusFound)
}

func AnswerList(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	answerQ(r.Form.Get("questionid"), r.Form.Get("ansnum"), r)
}
