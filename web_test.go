package web

// import (
// 	. "bitbucket.org/joyfulness/cbs/storage"
// 	"fmt"
// 	"github.com/gorilla/sessions"
// 	. "github.com/smartystreets/goconvey/convey"
// 	"github.com/zenazn/goji"
// 	"net/http"
// 	"net/http/httptest"
// 	"testing"
// )

// var userid = "13656"

// func TestRegister(t *testing.T) {
// 	Convey("Clear Database", t, func() {

// 		DropSchema()
// 		CreateSchema()
// 		CreateIndex()

// 		Reset(func() {
// 			TruncateAll()
// 		})
// 	})

// 	Convey("Create New User", t, func() {

// 		Convey("Complete & Go To Quiz", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Add("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("part", "สาขาย่อยวงศ์สว่างทาวน์ฯ")
// 			r.Form.Set("branch", "กทม.และปริมณฑล 1")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get(`Location`), ShouldEqual, `/quiz`)
// 			// So(string(w.Body.Bytes()), ShouldEqual, "/quiz")
// 		})

// 		Convey("Error & With Out Data", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Del("id")
// 			r.Form.Set("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("branch", "onnuch")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")

// 			r.Form.Set("curworkMonth", "3")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Del("department")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("branch", "onnuch")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Set("id", userid)
// 			r.Form.Add("department", "debt")
// 			r.Form.Del("name")
// 			r.Form.Set("branch", "onnuch")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Set("id", userid)
// 			r.Form.Set("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Del("branch")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Set("id", userid)
// 			r.Form.Set("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("branch", "onnuch")
// 			r.Form.Del("workYear")
// 			r.Form.Del("workMonth")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get(`Location`), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Set("id", userid)
// 			r.Form.Set("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("branch", "onnuch")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Del("curworkYear")
// 			r.Form.Del("curworkMonth")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")

// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get(`Location`), ShouldEqual, "")

// 			r, _ = http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Set("id", userid)
// 			r.Form.Set("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("part", "กทม.และปริมณฑล 1")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.ParseForm()
// 			w = httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get(`Location`), ShouldEqual, "")

// 		})
// 	})

// 	Convey("Get Question", t, func() {})

// }

// func TestContinueExam(t *testing.T) {
// 	Convey("Clear Database", t, func() {

// 		DropSchema()
// 		CreateSchema()
// 		CreateIndex()

// 		Reset(func() {
// 			TruncateAll()
// 		})
// 	})

// 	Convey("Answer Test", t, func() {

// 		Convey("Complete Register", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Add("department", "debt")
// 			r.Form.Add("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Add("part", "สาขาย่อยวงศ์สว่างทาวน์ฯ")
// 			r.Form.Add("branch", "กทม.และปริมณฑล 1")
// 			r.Form.Add("workYear", "4")
// 			r.Form.Add("workMonth", "3")
// 			r.Form.Add("curworkYear", "3")
// 			r.Form.Add("curworkMonth", "3")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get(`Location`), ShouldEqual, `/quiz`)
// 		})

// 		Convey("Continue Exam with Random UserID", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", "12343534534122")
// 			r.Form.Add("formname", "continue")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "/")
// 		})

// 		Convey("Continue Exam with UserID", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Add("formname", "continue")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Header().Get("Location"), ShouldEqual, "/quiz")
// 		})

// 	})
// }

// func TestReport(t *testing.T) {
// 	Convey("Clear Database", t, func() {

// 		DropSchema()
// 		CreateSchema()
// 		CreateIndex()

// 		Reset(func() {
// 			TruncateAll()
// 		})
// 	})

// 	Convey("Create New User", t, func() {

// 		Convey("Complete & Go To Quiz", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Add("department", "debt")
// 			r.Form.Set("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("part", "สาขาย่อยวงศ์สว่างทาวน์ฯ")
// 			r.Form.Set("branch", "กทม.และปริมณฑล 1")
// 			r.Form.Set("workYear", "4")
// 			r.Form.Set("workMonth", "3")
// 			r.Form.Set("curworkYear", "3")
// 			r.Form.Set("curworkMonth", "3")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get(`Location`), ShouldEqual, `/quiz`)
// 			// So(string(w.Body.Bytes()), ShouldEqual, "/quiz")
// 		})

// 		Convey("Answer Question 1-15", func() {
// 			for i := 1; i <= 15; i++ {

// 				r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 				r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 				r.ParseForm()
// 				r.Form.Add("formname", "answer")

// 				// r.Form.Add("answer", "1")

// 				w := httptest.NewRecorder()
// 				session, _ := store.Get(r, "cbs")
// 				session.Values["user_id"] = userid
// 				session.Values["curquestion_id"] = fmt.Sprintf("r1debtf%d", i)
// 				session.Save(r, w)

// 				goji.DefaultMux.ServeHTTP(w, r)

// 				So(w.Header().Get("Location"), ShouldEqual, "/quiz")
// 			}
// 		})

// 		// Convey("Report Success", func() {
// 		// 	urlPath := fmt.Sprintf("/r1/13656/report", userid)
// 		// 	r, _ := http.NewRequest("GET", urlPath, nil) // <-- URL-encoded payload
// 		// 	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

// 		// 	w := httptest.NewRecorder()
// 		// 	goji.DefaultMux.ServeHTTP(w, r)
// 		// 	So(w.Code, ShouldEqual, 302)
// 		// 	So(w.Header().Get("Location"), ShouldEqual, urlPath)
// 		// })

// 		Convey("Report With Random User ID", func() {
// 			urlPath := "/r1/999999/report"
// 			r, _ := http.NewRequest("GET", urlPath, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get("Location"), ShouldEqual, "/")
// 		})

// 	})
// }

// func TestAnswer(t *testing.T) {
// 	var store = sessions.NewFilesystemStore("./session", []byte("secretcbs"))

// 	Convey("Clear Database", t, func() {

// 		DropSchema()
// 		CreateSchema()
// 		CreateIndex()

// 		Reset(func() {
// 			TruncateAll()
// 		})
// 	})

// 	Convey("Answer", t, func() {
// 		Convey("Register Complete & Go To Quiz", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("id", userid)
// 			r.Form.Add("department", "debt")
// 			r.Form.Add("name", "พรทิพย์ อังคณาวรกุล")
// 			r.Form.Set("part", "สาขาย่อยวงศ์สว่างทาวน์ฯ")
// 			r.Form.Set("branch", "กทม.และปริมณฑล 1")
// 			r.Form.Add("workYear", "4")
// 			r.Form.Add("workMonth", "3")
// 			r.Form.Add("curworkYear", "3")
// 			r.Form.Add("curworkMonth", "3")
// 			r.ParseForm()
// 			w := httptest.NewRecorder()
// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get(`Location`), ShouldEqual, `/quiz`)
// 			// So(string(w.Body.Bytes()), ShouldEqual, "/quiz")
// 		})

// 		Convey("Answer Question", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("formname", "answer")

// 			r.Form.Add("answer", "1")

// 			w := httptest.NewRecorder()
// 			session, _ := store.Get(r, "cbs")
// 			session.Values["user_id"] = userid
// 			session.Values["curquestion_id"] = "r1actq13"
// 			session.Save(r, w)

// 			goji.DefaultMux.ServeHTTP(w, r)

// 			So(w.Header().Get("Location"), ShouldEqual, "/quiz")
// 		})

// 		Convey("Answer Question With No Session UserID", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("formname", "answer")
// 			r.Form.Add("answer", "1")

// 			w := httptest.NewRecorder()
// 			session, _ := store.Get(r, "cbs")
// 			session.Values["user_id"] = ""
// 			session.Values["curquestion_id"] = "r1actq13"
// 			session.Save(r, w)

// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get("Location"), ShouldEqual, "/")
// 		})

// 		Convey("Answer Question With No Session QuestionID", func() {
// 			r, _ := http.NewRequest("POST", `/`, nil) // <-- URL-encoded payload
// 			r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
// 			r.ParseForm()
// 			r.Form.Add("formname", "answer")
// 			r.Form.Add("answer", "1")

// 			w := httptest.NewRecorder()
// 			session, _ := store.Get(r, "cbs")
// 			session.Values["user_id"] = userid
// 			session.Values["curquestion_id"] = ""
// 			session.Save(r, w)

// 			goji.DefaultMux.ServeHTTP(w, r)
// 			So(w.Code, ShouldEqual, 302)
// 			So(w.Header().Get("Location"), ShouldEqual, "/")
// 		})

// 	})
// }

// // render template have problem
