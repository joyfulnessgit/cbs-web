package web

import (
	"bitbucket.org/joyfulness/cbs/service"
	ace "bitbucket.org/plimble/ace"
	"github.com/zenazn/goji/param"
	"github.com/zenazn/goji/web"
	"net/http"
)

// var store = sessions.NewFilesystemStore("./session", []byte("secretcbs"))

func GetStaticFile(c web.C, w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, r.URL.Path[1:])
}

func Webmain(c web.C, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)
	flashes := session.Flashes()
	session.Save(r, w)
	p := &struct {
		Message        interface{}
		Form           service.QuizProfileForm
		BranchList     interface{}
		DepartmentList interface{}
		FlashMessage   interface{}
		RoundList      service.Rounds
	}{BranchList: service.BranchList, DepartmentList: service.DepartmentList, RoundList: service.RoundList, Message: map[string]string{}, FlashMessage: flashes}
	ace.ServeTemplate(w, `master`, p)

}

func ManagePost(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.FormValue("formname") == "answer" {
		// FOR GET NEW QUESTION
		finishExam(c, w, r)
	} else if r.FormValue("formname") == "continue" {
		// FOR CONTINUE EXAM
		continueExam(c, w, r)
	} else if r.FormValue("formname") == "result" {
		// FOR USER REPORT
		if r.FormValue("rounds") == "" || r.FormValue("id") == "" {
			saveFlash("กรุณาระบุข้อมูลรหัสพนักงาน", w, r)
			http.Redirect(w, r, "/", http.StatusFound)
		}
		urlPath := "/" + r.FormValue("rounds") + "/" + r.FormValue("id") + "/report"
		http.Redirect(w, r, urlPath, http.StatusFound)
	} else {
		// REGISTER & BEGIN EXAM
		BeginExam(c, w, r)
	}

}

func BeginExam(c web.C, w http.ResponseWriter, r *http.Request) {

	if service.IsRoundExpired(service.GetCurrentRound()) {
		saveFlash("ท่านไม่สามารถทำแบบทดสอบได้เนื่องจากอยู่นอกเหนือเวลาที่กำหนด", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	}

	r.ParseForm()
	formData := service.QuizProfileForm{}

	ModFormData(r)
	errx := param.Parse(r.Form, &formData)
	if errx != nil {

	}
	qs := service.NewQuizService()
	user, err := qs.SaveQuizProfile(&formData)

	if err == nil && r.Form.Get("department") == "zetc" {
		saveFlash("ได้รับข้อมูลของคุณเรียบร้อยแล้ว ขอขอบคุณในการให้ความร่วมมือ", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	} else if err != nil && r.Form.Get("department") == "zetc" && err.GetMessage() == "user is already registered but not completed" {
		saveFlash("ท่านกรอกช้อมูลส่วนตัวเรียบร้อยแล้ว ไม่สามารถกรอกซ้ำได้", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	} else if err != nil && err.GetMessage() == "user is already registered but not completed" {
		saveFlash("ท่านได้เคยลงทะเบียนทำแบบทดสอบเป็นที่เรียบร้อยแล้ว", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	} else if err != nil && err.GetMessage() == "user is already finished quiz" {
		saveFlash("ท่านทำแบบทดสอบเสร็จเรียบแล้วแล้ว ไม่สามารถทำซ้ำได้", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	} else if err != nil {
		err.IsValidationType()
		p := &struct {
			BranchList     interface{}
			DepartmentList interface{}
			Message        interface{}
			Form           service.QuizProfileForm
			FlashMessage   interface{}
			RoundList      service.Rounds
		}{BranchList: service.BranchList, DepartmentList: service.DepartmentList, Message: err.GetMessage(), Form: formData, RoundList: service.RoundList, FlashMessage: ``}
		ace.ServeTemplate(w, `master`, p)
		return
	} else {
		saveUserSession(user, w, r)
		ace.NoCache(w)
		http.Redirect(w, r, "/quiz", http.StatusFound)
	}
}

func GetNameUser(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	name := service.GetUserName(r.Form.Get("userid"))
	p := map[string]string{"username": name}
	ace.ServeJson(200, w, p)
}

func GetDetailBranch(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	branch := r.Form.Get("region")
	p := service.BranchList[branch]
	ace.ServeJson(200, w, p)
}
