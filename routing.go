package web

import (
	"bitbucket.org/joyfulness/cbs/service"
	"bitbucket.org/joyfulness/cbs/storage"
	ace "bitbucket.org/plimble/ace"
	"bitbucket.org/plimble/ace/middleware"
	"fmt"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web"
	gm "github.com/zenazn/goji/web/middleware"
	"html/template"
	"os"
)

func init() {
	service.App.Storage = storage.NewStorage()

	goji.Use(gm.EnvInit)
	goji.Abandon(gm.Logger)
	goji.Abandon(gm.AutomaticOptions)
	goji.Abandon(gm.RequestID)
	goji.Use(middleware.GZip)

	goji.Get(`/`, Webmain)
	goji.Get(`/quiz`, WebListquiz)
	goji.Get(`/:rounds/:user_id/report`, Webreport)
	goji.Get(`/:rounds/:user_id/answer`, WebListQuizWithAnswer)

	w := web.New()
	goji.Handle(`/admin/*`, w)
	w.Use(middleware.BasicAuth(`test:test`))

	// static file
	goji.Get("/public/assets/*", GetStaticFile)

	// API
	goji.Post(`/`, ManagePost)
	goji.Post(`/quiz`, AnswerList)
	goji.Post(`/getNameUser`, GetNameUser)
	goji.Post(`/getDetailBranch`, GetDetailBranch)

	// BACK ENG REPORT
	w.Get(`/admin/reportallpdf`, WebReportPDF)
	w.Get(`/admin/listnoemp`, WebListNoEmp)
	w.Post(`/admin/listnoemp`, ServiceListNoEmp)
	w.Get(`/admin/listemp`, WebListEmp)
	w.Post(`/admin/servicelistemp`, ServiceListReportDay)

	w.Get(`/admin/report`, WebChart1)
	goji.Post(`/report/barline`, ServiceReportBarLine)
	goji.Post(`/report/allreport`, ServiceAllReport)

	// gen report
	w.Get(`/admin/gen`, GenReportAdmin)

	ace.SetSession(&ace.SessionConfig{
		SessionStore:        ace.SESSION_FILESYSTEM,
		SessionEncryptKey:   "secretcbs",
		SessionName:         "cbs",
		SessionRedisAddress: "",
		SessionFilePath:     "./session",
		MaxAge:              0,
	})

	funcMap := template.FuncMap{
		"getfulldate":           GetDateFromMonth,
		"getlocaltime":          GetTimeLocal,
		"getfullnamedepartment": GetFullNameDepartMent,
		"addnum":                AddNumTitle,
		"getpassstatus":         GetPassStatus,
		"checkround":            CheckRound,
		"getnumAnswer":          GetNumAnswer,
		"html": func(value interface{}) template.HTML {
			return template.HTML(fmt.Sprint(value))
		}}
	ace.SetTemplate(&ace.TemplateConfig{TemplatePath: os.Getenv(`CBS_TEMPLATE_PATH`), TemplateFuncs: funcMap})

}
