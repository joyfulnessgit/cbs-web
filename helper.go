package web

import (
	"bitbucket.org/joyfulness/cbs/service"
	ace "bitbucket.org/plimble/ace"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"
)

func saveUserSession(user *service.User, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)
	session.Values["user_id"] = user.ID
	session.Values["is_done"] = user.IsDone
	session.Values["round"] = user.Round
	session.Values["dept"] = user.Department
	session.Save(r, w)
}

func ModFormData(r *http.Request) {
	r.Form.Add("work_exp", GetMonthExp(r.FormValue("workYear"), r.FormValue("workMonth")))
	r.Form.Add("curpos_exp", GetMonthExp(r.FormValue("curworkYear"), r.FormValue("curworkMonth")))
	r.Form.Add("act_exp", GetMonthExp(r.FormValue("actYear"), r.FormValue("actMonth")))
	r.Form.Add("crd_exp", GetMonthExp(r.FormValue("crdYear"), r.FormValue("crdMonth")))
	r.Form.Add("fin_exp", GetMonthExp(r.FormValue("finYear"), r.FormValue("finMonth")))
	r.Form.Add("leg_exp", GetMonthExp(r.FormValue("legYear"), r.FormValue("legMonth")))
	r.Form.Add("debt_exp", GetMonthExp(r.FormValue("debtYear"), r.FormValue("debtMonth")))
	r.Form.Add("other_exp", GetMonthExp(r.FormValue("otherYear"), r.FormValue("otherMonth")))
	r.Form.Add("mgt_exp", GetMonthExp(r.FormValue("mgtYear"), r.FormValue("mgtMonth")))

	r.Form.Del("workYear")
	r.Form.Del("workMonth")

	r.Form.Del("otherYear")
	r.Form.Del("otherMonth")

	r.Form.Del("mgtYear")
	r.Form.Del("mgtMonth")

	r.Form.Del("curworkYear")
	r.Form.Del("curworkMonth")

	r.Form.Del("actYear")
	r.Form.Del("actMonth")

	r.Form.Del("crdYear")
	r.Form.Del("crdMonth")

	r.Form.Del("finYear")
	r.Form.Del("finMonth")

	r.Form.Del("legYear")
	r.Form.Del("legMonth")

	r.Form.Del("debtYear")
	r.Form.Del("debtMonth")
}

func GetMonthExp(year string, month string) string {
	tempYear, _ := strconv.Atoi(year)
	tempMonth, _ := strconv.Atoi(month)
	tempYear = tempYear * 12
	return strconv.Itoa(tempYear + tempMonth)
}

func AddNumTitle(i int) int {
	return i + 1
}

func GetFullNameDepartMent(shortname string) string {
	fullname := ""
	if shortname == "crd" {
		fullname = "สินเชื่อ"
	} else if shortname == "debt" {
		fullname = "บริหารหนี้"
	} else if shortname == "fin" {
		fullname = "การเงิน"
	} else if shortname == "leg" {
		fullname = "นิติกรรม"
	} else if shortname == "act" {
		fullname = "บัญชี"
	} else if shortname == "mgt" {
		fullname = "ผู้ช่วย/ผู้จัดการสาขา"
	}
	return fullname
}

func GetPassStatus(status bool) template.HTML {
	if status {
		return template.HTML(fmt.Sprint("<div class=\"green-label\"><b style='color:white;'>ผ่านการทดสอบ </b></div>"))
	} else {
		return template.HTML(fmt.Sprint("<div class=\"red-label\"><b style='color:white;'>ไม่ผ่านการทดสอบ </b></div>"))
	}
}

func CheckRound(roundName string) bool {
	if roundName == "r0" {
		return false
	} else {
		return true
	}
}

func GetTimeLocal(timeNow time.Time) string {
	return fmt.Sprintf("%02d-%02d-%d",
		timeNow.Local().Day(),
		timeNow.Local().Month(),
		timeNow.Local().Year())
}

func GetDateFromMonth(month int) string {
	resultYear := month / 12
	// fmt.Println("Month : ", month, " RESULT YEAR : ", resultYear, " MONTH : ", month-(resultYear*12))
	return fmt.Sprintf("%d ปี %d เดือน", resultYear, month-(resultYear*12))
}

func GetNumAnswer(num int) string {
	switch num {
	case 1:
		return "ก."
	case 2:
		return "ข."
	case 3:
		return "ค."
	case 4:
		return "ง."
	}
	return ""
}

func saveFlash(text string, w http.ResponseWriter, r *http.Request) {
	session, _ := ace.Session(r)

	// flashMessage := &struct {
	//  messageRegister string
	//  messageContinue string
	//  messageReport   string
	// }{messageRegister: "1", messageContinue: "2", messageReport: "2"}
	session.AddFlash(text)
	session.Save(r, w)
}
