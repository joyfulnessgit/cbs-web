package web

import (
	"bitbucket.org/joyfulness/cbs/service"
	ace "bitbucket.org/plimble/ace"
	"fmt"
	"github.com/zenazn/goji/web"
	"net/http"
	"time"
)

func GenReportAdmin(c web.C, w http.ResponseWriter, r *http.Request) {
	rs := service.NewReportService()
	err := rs.GenerateReport()
	if err != nil {
		ace.ServeText(200, w, err.Error())
	}
	ace.ServeText(200, w, "GENERATE REPORT SUCCESS")

	// err := rs.GenerateReport()
}

func Webreport(c web.C, w http.ResponseWriter, r *http.Request) {
	qr := service.NewReportService()
	user, err := qr.GetUserReport(c.URLParams["user_id"], c.URLParams["rounds"])
	expDate := getExpDate()

	if err != nil {
		saveFlash("ไม่พบข้อมูลผู้ใช้", w, r)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if user.IsDone == false && user.Department == "zetc" {
		saveFlash("ท่านไม่สามารถตรวจสอบผลคะแนนได้ เนื่องจากไม่ได้อยู่ในกลุ่มงานที่กำหนด", w, r)
		// http.Redirect(w, r, "/quiz", http.StatusFound)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if user.IsDone == false {
		saveFlash("ไม่สามารถดูผลคะแนนได้ กรุณาทำแบบทดสอบให้ครบทุกข้อ ก่อนส่งคำตอบ", w, r)
		r.ParseForm()
		r.Form.Add("id", c.URLParams["user_id"])
		continueExam(c, w, r)
		return
	}
	session, _ := ace.Session(r)
	flashes := session.Flashes()
	fmt.Println(flashes)

	disabledbutton := service.IsRoundExpired(service.GetCurrentRound())

	p := &struct {
		User         *service.User
		ExpDate      string
		EnableButton bool
	}{User: user, ExpDate: expDate, EnableButton: disabledbutton}

	ace.NoCache(w)
	ace.ServeTemplate(w, `report`, p)
}

func getExpDate() string {
	r := service.GetCurrentRound()
	timeExp := service.GetRoundExpired(r)
	return fmt.Sprintf("%02d-%02d-%d",
		timeExp.Local().Day()+1,
		timeExp.Local().Month(),
		timeExp.Local().Year())
}

func WebListEmp(c web.C, w http.ResponseWriter, r *http.Request) {
	p := &struct {
		UserList []*service.User
	}{UserList: []*service.User{}}
	ace.ServeTemplate(w, `listemp`, p)
}

func ServiceListNoEmp(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form.Get("datereport") != "" {
		const shortForm = "2 Jan 2006"
		t, _ := time.Parse(shortForm, r.Form.Get("datereport"))
		qr := service.NewReportService()

		dailyreport, err := qr.GetDialyReport(t)
		if err == nil {
			p := &struct {
				FinishUser      []*service.User
				NonRegisterUser map[string]string
				OtherUserNum    int
				Passednum       int
				Failednum       int
			}{FinishUser: dailyreport.Finished, NonRegisterUser: dailyreport.NonRegister, OtherUserNum: dailyreport.Etc, Passednum: dailyreport.Passed, Failednum: dailyreport.Failed}
			ace.ServeTemplate(w, `listnoemp`, p)
		}
	} else {
		p := &struct {
			FinishUser      []*service.User
			NonRegisterUser map[string]string
			OtherUserNum    int
			Passednum       int
			Failednum       int
		}{FinishUser: []*service.User{}, NonRegisterUser: map[string]string{}, OtherUserNum: 0, Passednum: 0, Failednum: 0}

		ace.ServeTemplate(w, `listnoemp`, p)
	}
}

func WebListNoEmp(c web.C, w http.ResponseWriter, r *http.Request) {
	p := &struct {
		FinishUser      []*service.User
		NonRegisterUser map[string]string
		OtherUserNum    int
		Passednum       int
		Failednum       int
	}{FinishUser: []*service.User{}, NonRegisterUser: map[string]string{}, OtherUserNum: 0, Passednum: 0, Failednum: 0}

	ace.ServeTemplate(w, `listnoemp`, p)

}

func ServiceListReportDay(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	const shortForm = "2 Jan 2006"
	t, _ := time.Parse(shortForm, r.Form.Get("datereport"))
	us := service.NewUserService()
	listuser, err := us.GetDoneByDate(t)
	if err == nil {
		p := &struct {
			UserList []*service.User
		}{UserList: listuser}
		ace.ServeTemplate(w, `listemp`, p)
	}
}

func WebChart1(c web.C, w http.ResponseWriter, r *http.Request) {
	exptime, _ := time.Parse(time.RFC3339, service.RoundList.Rounds[service.RoundList.Current].Expired)
	starttime, _ := time.Parse(time.RFC3339, service.RoundList.Rounds[service.RoundList.Current].Started)

	p := &struct {
		Exptime    string
		Starttime  string
		Message    interface{}
		BranchList interface{}
		RoundList  service.Rounds
		Form       service.QuizProfileForm
	}{Exptime: GetTimeLocal(exptime), Starttime: GetTimeLocal(starttime), BranchList: service.BranchList, RoundList: service.RoundList, Message: map[string]string{}}

	ace.ServeTemplate(w, `chart1`, p)
}

func ServiceAllReport(c web.C, w http.ResponseWriter, r *http.Request) {
	qr := service.NewReportService()
	round := service.GetCurrentRound()
	allpass, err := qr.GetAllReport(round)
	if err == nil {
		p := map[string]interface{}{"allpass": allpass}
		ace.ServeJson(200, w, p)
	}
}

func WebReportPDF(c web.C, w http.ResponseWriter, r *http.Request) {
	exptime, _ := time.Parse(time.RFC3339, service.RoundList.Rounds[service.RoundList.Current].Expired)
	starttime, _ := time.Parse(time.RFC3339, service.RoundList.Rounds[service.RoundList.Current].Started)

	p := &struct {
		Exptime    string
		Starttime  string
		Message    interface{}
		BranchList interface{}
		RoundList  service.Rounds
		Form       service.QuizProfileForm
	}{Exptime: GetTimeLocal(exptime), Starttime: GetTimeLocal(starttime), BranchList: service.BranchList, RoundList: service.RoundList, Message: map[string]string{}}

	ace.ServeTemplate(w, `chartall`, p)
}

func ServiceReportBarLine(c web.C, w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	qr := service.NewReportService()
	round := service.GetCurrentRound()

	if r.Form.Get("type") == "department" {
		dataPass, err := qr.GetAllDepartmentReport(round)

		if err != nil {
			p := map[string]interface{}{"error": err.Error()}
			ace.ServeJson(200, w, p)
			return
		}
		p := map[string]interface{}{"pass": dataPass}
		ace.ServeJson(200, w, p)

	} else if r.Form.Get("type") == "branch" {

		dataPass, err := qr.GetBranchReport(r.Form.Get("part"), r.Form.Get("branch"), round)
		if err != nil {
			p := map[string]interface{}{"error": err.Error()}
			ace.ServeJson(200, w, p)
			return
		}
		p := map[string]interface{}{"pass": dataPass}
		ace.ServeJson(200, w, p)

	} else if r.Form.Get("type") == "allbranch" {
		dataPass, err := qr.GetAllBranchReport(round)

		if err != nil {
			p := map[string]interface{}{"error": err.Error()}
			ace.ServeJson(200, w, p)
			return
		}
		p := map[string]interface{}{"pass": dataPass}
		ace.ServeJson(200, w, p)

	} else if r.Form.Get("type") == "part" {
		fmt.Println("part : ", r.Form.Get("part"))
		dataPass, err := qr.GetPartReport(r.Form.Get("part"), round)
		if err != nil {
			p := map[string]interface{}{"error": err.Error()}
			ace.ServeJson(200, w, p)
			return
		}
		p := map[string]interface{}{"pass": dataPass}
		ace.ServeJson(200, w, p)

	} else if r.Form.Get("type") == "allpart" {
		dataPass, err := qr.GetAllPartReport(round)
		if err != nil {
			p := map[string]interface{}{"error": err.Error()}
			ace.ServeJson(200, w, p)
			return
		}
		p := map[string]interface{}{"pass": dataPass}
		ace.ServeJson(200, w, p)

	} else {

	}

}
